﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string s = "Hello, " + txtUserName.Text;
            MessageBox.Show(s);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAbout fa = new frmAbout();
            fa.Show(this);
            this.Visible = false;
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            ;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
                button2.Left = button2.Left - 50;
            if (e.KeyCode == Keys.Right)
                button2.Left = button2.Left + 50;
        }
    }
}
